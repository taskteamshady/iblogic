<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    @include( 'inc.header' )
</head>
<body class="blue lighten-5">
@yield( 'content' )
<!-- Foot Scripts -->
@yield( 'scripts' )
</body>
</html>