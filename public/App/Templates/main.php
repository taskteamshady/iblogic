@extends( 'layouts.master' )
@section( 'title' )
QuickAPI Service
@stop
@section( 'content' )
<div class="container-fluid">
    <div class="row">
        <div class="col s8 push-s4">
            <div class="row">
                <div class="col s12 m6">
                    <div class="card medium deep-purple darken-1 floating">
                        <form>
                            <div class="card-content white-text">
                                <img class="mb-4 logo" src="assets/images/database.png" alt="" width="36" height="36">
                                <span class="card-title center">QuickAPI Service</span>
                                <h5 class="h4 mb-3 font-weight-normal center">Please, feel free to enter your data</h5>
                                <label for="inputEmail" class="sr-only">Email address</label>
                                <input type="text" id="inputEmail" class="form-control" placeholder="Email address"
                                       autofocus>
                                <label for="inputAmount" class="sr-only">Amount of money</label>
                                <input type="text" id="inputAmount" placeholder="99.99" class="form-control"
                                       placeholder="Amount" maxlength="10">
                            </div>
                            <div class="card-action">
                                <button class="btn medium center waves-effect waves-light red darken-4 addtrans"
                                        type="submit" name="action">Send
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    <a id="reportByMonth" class="waves-effect waves-light btn light-green"><i
                                class="material-icons left">assessment</i>Report by month</a>
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    <a id="reportByDays" class="waves-effect waves-light btn btn-block light-green"><i
                                class="material-icons left">assessment</i>Report by days</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="messageModal" class="modal large">
    <div class="modal-content">
        <h4>Answer</h4>
        <div class="description">A bunch of text</div>
    </div>
    <div class="modal-footer">
        <a href="" class="modal-close waves-effect waves-green btn-flat">Ok</a>
    </div>
</div>
@stop
@section( 'scripts' )
@include( 'inc.footer' )
@stop