<?php
/**
 * Created by PhpStorm.
 * User: vic
 */

namespace App\Views;


/**
 * Class ReportView
 * @package App\Views
 * Separate class to view reports
 */
class ReportView extends View
{
    /**
     * @param $data
     * Main method to return report table
     */
    public static function show($data) {
        ?>
        <table class="responsive-table highlight">
            <thead>
            <tr>
                <?php
                $thead = array_keys($data[0]);
                foreach ($thead as $th) {
                    echo '<th>',$th,'</th>';
                }
                ?>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $row) {
                echo '<tr>';
                foreach ($row as $r) {
                    echo '<td>',$r,'</td>';
                }
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>

        <?php
    }
}