<?php
/**
 * Created by PhpStorm.
 * User: vic
 */
namespace App\Views;

use App\Core\FileNotFoundException;
use RuntimeException;

/**
 * Class View
 * @package App\Views
 * Main View class to parse and render templates
 */
class View
{
    /**
     * @var
     * Template file name
     */
    protected $view_file;
    /**
     * @var
     * Template file address from TEMPLATES constant
     */
    protected $view_path;
    /**
     * @var string
     * Stores result html
     */
    protected $content = '';
    /**
     * @var array
     *
     */
    protected static $sections = [];
    /**
     * @var null
     * Array of template parts
     */
    protected static $shared = null;

    /**
     * View constructor.
     * @param $view_file
     * @param array $data
     * Stores data sent by controller
     */
    public function __construct($view_file, Array $data = [])
    {
        if (self::$shared === null) {
            // Share Data
            foreach ($data as $key => $value) {
                if (!isset(self::$shared[$key])) {
                    self::$shared[$key] = $value;
                }
            }
        }

        $this->view_file = $view_file;

        $this->render();

    }

    /**
     * @param $view_file
     * @param array $data
     * @return View
     * Just a constructor alias
     */
    public static function make($view_file, Array $data = [])
    {
        return new self($view_file, $data);
    }

    /**
     * Method to return data from file
     */
    protected function render()
    {
        $this->view_file = str_replace('.', DS, str_replace('.php', '', $this->view_file)) . '.php';

        $this->view_path = TEMPLATES . DS . $this->view_file;
        try {
            if (!file_exists($this->view_path)) {
                throw new FileNotFoundException('Template ' . str_replace(TEMPLATES . DS, '', $this->view_path) . ' not found!');
            }

        } catch (FileNotFoundException $f) {
            echo $f->getMessage(), '<br>Trace: ', '<hr><pre>';
            print_r($f->getTrace());
            echo '</pre>';
            exit();
        }
        //Get buffer cleaned
        $this->pre = ob_get_clean();
        ob_start();
        include($this->view_path);
        $this->content = ob_get_clean();
        $this->parse();
    }

    /**
     * Template parts parser
     */
    protected function parse()
    {
        $this->content = preg_replace_callback('/@section\(\s*([\'\"])([\w\-\d]+)\1\s*\)(.*?)\@stop/si',
            function ($match) {
                static::$sections[$match[2]] = $match[3];
                return '';
            },
            $this->content);

        // Check if has Parent View
        $this->content = preg_replace_callback('/@extends\(\s*([\'\"])([\w\.\-\d]+)\1\s*\)/si', function ($match) {
            $parent_view = new self($match[2]);
            $parent_view->content = preg_replace_callback('/@yield\(\s*([\'\"])([\w\-\d]*)\1\s*\)/si',
                function ($match) {
                    if (isset(static::$sections[$match[2]])) {
                        return static::$sections[$match[2]];
                    } else {
                        return null;
                    }
                },
                $parent_view->content);
            return $parent_view->content;
        },
            $this->content);
        // {{  }} Syntax
        $this->content = preg_replace_callback('/\{\{(.*?)\}\}/', function ($match) {
            return htmlspecialchars(eval('return ' . $match[1] . ';'));
        },
            $this->content);

        // {!!  !!}
        $this->content = preg_replace_callback('/\{!!(.*?)\!!\}/', function ($match) {
            return eval('return ' . $match[1] . ';');
        },

            $this->content);
        // @include
        $this->content = preg_replace_callback('/@include\(\s([\'\"])([\w\.]+?)\1\s\)/si', function ($match) {
            $parent_view = new self($match[2]);
            return $parent_view->content;
        },
            $this->content);
        // Add Previous Application Buffered Content If Exists
        if ($this->pre) {
            $this->content = $this->pre . $this->content;
        }

    }

    /**
     * @param $property
     * @return mixed
     * View property getter
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        } else {
            if (isset(static::$shared[$property])) {
                return static::$shared[$property];
            } else {
                throw new RuntimeException('Undefined property "' . $property . '"');
            }
        }
    }
}