<?php
/**
 * Created by PhpStorm.
 * User: vic
 */

namespace App\Controllers;

use App\Core\Response;

/**
 * Class IndexController
 * @package App\Controllers
 * Nothing but homepage
 */
class IndexController extends Controller
{
    /**
     * @return Response
     * Template name && append session
     */
    public function index()
    {
        return Response::view('main', [$this->session]);
    }
}