<?php
/**
 * Created by PhpStorm.
 * User: vic
 */

namespace App\Controllers;


use App\Core\Response;
use App\Models\Data;
use App\Views\ReportView;

/**
 * Class ApiController
 * It's all about api methods, direct access is not allowed
 * @package App\Controllers
 */
class ApiController extends Controller
{
    /**
     * Just a simple checking if script was accessed directly
     */
    private function checkAjax() {
        if (!$this->is_ajax) exit('Direct access is not allowed');
    }

    /**
     * @param $params
     * Method for new transactions
     * @return Response
     */
    public function transaction ($params) {
        self::checkAjax();
        $input = array(
            'email' => $params['email'],
            'amount' => $params['amount']
        );
        $data = new Data();
        $result = $data->insert($input);
        return Response::json($result);
    }

    /**
     * @param $params
     * Forwarding report requests to data model
     */
    public function getReport($params) {
        self::checkAjax();
        $type = $params['type'];
        $data = new Data();
        $report = $data->getReport($type);
        ReportView::show($report);
    }
}