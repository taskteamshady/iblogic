<?php
/**
 * Created by PhpStorm.
 * User: vic
 */
namespace App\Controllers;
use App\Core\Session;

/**
 * Class Controller
 * @package App\Controllers
 * Parent controller to extend
 */
class Controller
{
    /**
     * @var bool
     * Ajax property variable
     */
    protected $is_ajax;
    /**
     * @var Session
     * Session variable, useful usage is not implemented
     */
    protected $session;

    /**
     * Controller constructor.
     * Attach ajax property and session
     */
    public function __construct()
    {
        // Check AJAX
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
            $this->is_ajax = true;
        }
        $this->session = new Session();
    }
}