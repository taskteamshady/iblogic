<?php
/**
 * Created by PhpStorm.
 * User: vic
 */

namespace App\Models;

use Error;
use PDO;
use PDOException;
use Throwable;

/**
 * Class Model
 * @package App\Models
 * Main model to extend
 */
class Model
{
    /**
     * @var
     * Database object (PDO)
     */
    protected static $DBH;

    /**
     * @return PDO
     * Return existing database object or create a new one
     */
    public static function openConnection()
    {
        try {
            if (!isset(self::$DBH) || !(self::$DBH instanceof PDO)) {
                self::$DBH = new PDO('mysql:host=' . DB_HOSTNAME . ';dbname=' . DB_NAME . ';charset=utf8',
                    DB_USERNAME,
                    DB_USERPWD
                );
                self::$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            return self::$DBH;

        } catch (PDOException $e) {
            echo $e->getMessage(), '<br>Trace: ','<hr><pre>';
            print_r($e->getTrace()); echo '</pre>';
            exit();
        }
    }

    /**
     * @return bool
     * Forced removing PDO
     */
    public static function closeConnection()
    {
        if (isset(self::$DBH)) {
            self::$DBH = null;
        }

        return true;
    }

    /**
     * @param $sql
     * @param $options
     * Order, limit and offset for general select queries
     */
    private static function completeStatement(&$sql, &$options)
    {
        //Order option
        if (isset($options['order_by'])) {
            $sql .= ' ORDER BY ' . $options['order_by'];
            $sql .= (isset($options['order'])) ? ' ' . $options['order'] : null;
        }
        //Limit option
        $sql .= (isset($options['limit'])) ? ' LIMIT ' . $options['limit'] : null;
        //Offset option
        $sql .= (isset($options['offset'])) ? ' OFFSET ' . $options['offset'] : null;

        unset($options['limit'], $options['offset'], $options['order_by']);
    }

    /**
     * @param $sql
     * @param null $prepare_values
     * @param array $options
     * @return mixed
     * Method for general select queries
     */
    public static function runSql($sql, $prepare_values = null, Array $options = array())
    {
        if (is_null(self::$DBH)) {
            self::openConnection();
        }
        self::completeStatement($sql, $options);

        try {
        if ($prepare_values) {
            $query = self::$DBH->prepare($sql);
            $query->execute($prepare_values);
        } else {
            $query = self::$DBH->query($sql);
        }

        $set = ($options['fetch_all']) ? $query->fetchAll() : $query->fetch();
        } catch (PDOException $e) {
            echo $e->getMessage(), '<br>Trace: ','<hr><pre>';
            print_r($e->getTrace()); echo '</pre>';
            exit();
        } catch (Error $e) {
            echo $e->getMessage(), '<br>Trace: ','<hr><pre>';
            print_r($e->getTrace()); echo '</pre>';
            exit();
        } catch (Throwable $t) {
            echo $t->getMessage(), '<br>Trace: ','<hr><pre>';
            print_r($t->getTrace()); echo '</pre>';
            exit();
        }

        return $set;

    }
}