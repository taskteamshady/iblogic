<?php
/**
 * Created by PhpStorm.
 * User: vic
 */

namespace App\Models;

use PDO;
use PDOException;

/**
 * Class Data
 * @package App\Models
 * Main data access model
 */
class Data extends Model
{
    /**
     * @var string
     * Default table in database
     */
    protected static $table_name = 'transactions';
    /**
     * @var string
     * Default class name (data, may differ)
     */
    protected static $class = __CLASS__;
    /**
     * @var array
     * Fields from table field
     */
    protected static $fields = [
        'id',
        'transaction_id',
        'email',
        'amount',
        'status',
        'create_date'
    ];

    /**
     * Method for creating table if not exist
     */
    private function createSchema()
    {
        $sql = "CREATE TABLE `" . self::$table_name . "`
            (
                id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
                transaction_id VARCHAR(64),
                email VARCHAR(255) NOT NULL,
                amount NUMERIC(10,2) NOT NULL,
                status ENUM('rejected', 'approved'),
                create_date TIMESTAMP DEFAULT NOW()
            )";
        $connection = parent::openConnection();
        try {
            $query = $connection->prepare($sql);
            $query->execute();
        } catch (PDOException $e) {
            echo $e->getMessage(), '<br>Trace: ', '<hr><pre>';
            print_r($e->getTrace());
            echo '</pre>';
            exit();
        }
    }

    /**
     * @return bool
     * Check existing table in database
     */
    private function checkTable()
    {
        $sql = "SELECT COUNT(table_name) exist 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE table_name = 'transactions'";
        try {
            $connection = parent::openConnection();
            $exist = $connection->query($sql)->fetch(PDO::FETCH_ASSOC)['exist'];
        } catch (PDOException $e) {
            echo $e->getMessage(), '<br>Trace: ', '<hr><pre>';
            print_r($e->getTrace());
            echo '</pre>';
            exit();
        }
        return $exist == '1' ? true : false;
    }

    /**
     * @return string
     * Make random transaction id
     */
    private function getTransId()
    {
        return md5(@session_id() . date('H:i:s'));
    }

    /**
     * @param array $args
     * @return array
     * Main method to insert record and return an api response
     */
    public function insert(Array $args)
    {
        if (!self::checkTable()) self::createSchema();
        $sql = "INSERT INTO transactions (transaction_id,email,amount,status) VALUES (?,?,?,?)";
        $transId = self::getTransId();
        $responses = array(
            'rejected' => 'Fraud detected!',
            'approved' => 'Success!'
        );
        $statuses = array_keys($responses);
        $random = $statuses[array_rand($statuses)];
        try {
            $connection = parent::openConnection();
            $query = $connection->prepare($sql);
            //Setting values
            $query->bindParam(1, $transId, PDO::PARAM_STR);
            $query->bindParam(2, $args['email'], PDO::PARAM_STR);
            $query->bindParam(3, number_format($args['amount'], 2, '.', ''), PDO::PARAM_STR);
            $query->bindParam(4, $random, PDO::PARAM_STR);
            $query->execute();
        } catch (PDOException $e) {
            echo $e->getMessage(), '<br>Trace: ', '<hr><pre>';
            print_r($e->getTrace());
            echo '</pre>';
        }
        return array(
            'transaction_id' => $transId,
            'status' => $random,
            'message' => $responses[$random]
        );

    }

    /**
     * @param $type
     * @return array|bool
     * Make a report from table or return nothing
     */
    public function getReport($type)
    {
        switch ($type) {
            case 'general':
                $sql = "SELECT email `Email`,
                               SUM(amount) `Summary`
                        FROM transactions
                        WHERE MONTH(create_date) = MONTH(NOW())
                        GROUP BY email";
                break;
            case 'byday':
                $sql = "SELECT email `Email`,
                SUM( IF( DAYOFWEEK( create_date ) = 2, amount, 000000.00 )) as `Monday`,
                SUM( IF( DAYOFWEEK( create_date ) = 3, amount, 000000.00 )) as `Tuesday`,
                SUM( IF( DAYOFWEEK( create_date ) = 4, amount, 000000.00 )) as `Wednesday`,
                SUM( IF( DAYOFWEEK( create_date ) = 5, amount, 000000.00 )) as `Thursday`,
                SUM( IF( DAYOFWEEK( create_date ) = 6, amount, 000000.00 )) as `Friday`,
                SUM( IF( DAYOFWEEK( create_date ) = 7, amount, 000000.00 )) as `Saturday`,
                SUM( IF( DAYOFWEEK( create_date ) = 1, amount, 000000.00 )) as `Sunday`
                FROM transactions
                GROUP BY email";
                break;

        }
        try {
            $connection = parent::openConnection();
            $query = $connection->query($sql);
            $set = $query->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo $e->getMessage(), '<br>Trace: ', '<hr><pre>';
            print_r($e->getTrace());
            echo '</pre>';
        }
        if (!empty($set)) {
            return $set ? $set : false;
        }
    }

}