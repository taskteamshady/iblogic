<?php
/**
 * Created by PhpStorm.
 * User: vic
 */

namespace App\Core;

class Session
{
    protected $user = null;
    public $sessionId;

    /**
     * Constructor
     */
    public function __construct()
    {
        session_name('iblogic');
        @session_start();
        $this->sessionId = session_id();
    }

    public function set($key, $value)
    {
        $_SESSION[$key] = $value;

        return $this;
    }

    public function has($key, $subkey = null)
    {
        if ($subkey) {
            return isset($_SESSION[$key][$subkey]);
        } else {
            return isset($_SESSION[$key]);
        }
    }

    public function get($key, $default = null)
    {
        return $this->has($key) ? $_SESSION[$key] : $default;
    }

    public function pull($key, $default = null)
    {
        if ($val = $this->get($key)) {
            $this->forget($key);

            return $val;
        }

        return $default;
    }

    public function forget($key)
    {
        if ($this->has($key)) {
            unset($_SESSION[$key]);
        }

        return $this;
    }

    public function destroy()
    {
        unset($_SESSION);
        session_destroy();
    }
}
