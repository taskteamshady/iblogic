<?php
/**
 * Created by PhpStorm.
 * User: vic
 */

namespace App\Core;

class Router
{
    protected $controller;
    protected $inputs = [];
    protected $is_ajax = false;
    protected $method;
    protected $parameters = [];
    protected $request_method = 'get';
    protected $request_result;
    protected $request_uri;
    protected $segments = [];

    public function __construct()
    {
        $this->request_uri = $_SERVER['REQUEST_URI'];
        // Check method
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                $this->request_method = 'get';
                break;
            case 'POST':
                $this->request_method = 'post';
                break;
        }
        //Input values
        $this->setInputs();
        //Resolve index request
        if ($this->request_uri === '/' || $this->request_uri === '/index.php') {
            $this->request_uri = '/index/index';
        }

        if (preg_match('/transaction/', $this->request_uri) > 0) {
            $this->request_uri = str_replace('/transaction', '/api/transaction', $this->request_uri);
        }
        //Find a controller
        $this->analyseRequest();
    }

    protected function analyseRequest()
    {
        $this->segments = $this->request_uri ? explode('/', rtrim($this->request_uri, '/')) : [];
        //Remove empty space from the beginning
        array_shift($this->segments);
        //Find controller
        $this->setController();
        // Load Controller
        $this->load();

    }

    /**
     * Extract Controller Name, Method, Parameters
     */
    protected function setController()
    {
        if (!empty($this->segments)) {
            $this->controller = ucfirst($this->checkSegmentName($this->segments[0])) . 'Controller';
            $controller_path = CONT . DS . $this->controller . '.php';
            if (!file_exists($controller_path) || $controller_path === 'Controller.php') {
                Response::view('404', ['code' => 404], 404)->send();
            }
            $count = count($this->segments);
            switch ($count) {
                case 1:
                    $this->method = 'index';
                    break;
                default:
                    $this->method = $this->checkSegmentName($this->segments[1]);
                    break;
            }
        }
    }


    protected function load()
    {
        // Get controller
        $controller_name = 'App\Controllers\\' . $this->controller;
        $controller = new $controller_name();
        // Check Method
        if (!method_exists($controller, $this->method)) {
            Response::view('404', ['code' => 404], 404)->send();
        }
        // Call Method
        $this->request_result = call_user_func_array([$controller, $this->method], array($this->all()));
        // Make Response From Controller Result, and send it to Browser;
        Response::make($this->request_result)->send();

    }

    protected function checkSegmentName($name)
    {
        $name = strtolower($name);

        $name = preg_replace_callback('/\-([a-zA-Z]{1})/i', function ($arr) {
            return ucfirst($arr[1]);
        }, $name);

        return $name;
    }

    protected function setInputs()
    {
        foreach ($_GET as $input => $value) {
            $this->inputs[$input] = trim($value);
        }

        foreach ($_POST as $input => $value) {
            $this->inputs[$input] = !is_array($value) ? trim($value) : $value;
        }
    }

    public function input($key, $default = null)
    {
        // Get Input value
        if ($this->has($key)) {
            return $this->inputs[$key];
        } else {
            return $default;
        }
    }

    public function all()
    {
        return $this->inputs;
    }

    public function has($key)
    {
        return isset($this->inputs[$key]);
    }
}