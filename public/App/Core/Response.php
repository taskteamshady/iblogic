<?php
/**
 * Created by PhpStorm.
 * User: vic
 */

namespace App\Core;


use App\Views\View;

/**
 * Class Response
 * @package App\Core
 * Wrapping class for all responses to user
 */
class Response
{
    /**
     * @var
     * Stores protocol information
     */
    protected static $server_protocol;
    /**
     * @var string
     * Default content type
     */
    protected $content_type = 'text/html';
    /**
     * @var int
     * Default status code
     */
    protected $status_code = 200;
    /**
     * @var array
     * Default messages for status codes
     */
    protected $status_text = [
        200 => 'OK',
        404 => 'Not Found',
        405 => 'Method Not allowed',
    ];
    /**
     * @var string
     * Status message
     */
    protected $header_status;
    /**
     * @var string
     * This is most important field, because it stores html data
     */
    protected $content = '';

    /**
     * Response constructor.
     * @param $content
     * @param int $code
     * @param array|null $headers
     * Sets content, default code is OK and headers are empty
     */
    public function __construct($content, $code = 200, Array $headers = null)
    {
        static::$server_protocol = $_SERVER["SERVER_PROTOCOL"];
        $this->content = $content;
        $this->status_code = $code;

        $this->header_status = static::$server_protocol . ' ' . $this->status_code . ' ' . @$this->status_text[$this->status_code];
        $this->setHeader($this->header_status);

        if (is_array($headers)) {
            foreach ($headers as $key => $value) {
                $this->setHeader($key, $value);
            }
        }
    }

    /**
     * @param $raw_string
     * @param int $code
     * @param array $headers
     * @return Response
     * An alias for not formatted html
     */
    public static function raw($raw_string, $code = 200, Array $headers = [])
    {
        return new self(
            $raw_string,
            $code,
            array_merge(['Content-Type' => 'text/html'], $headers)
        );

    }

    /**
     * @param $array
     * @param int $code
     * @param array $headers
     * @return Response
     * Response function for json-based content
     */
    public static function json($array, $code = 200, Array $headers = [])
    {
        self::checkArrayValuesForJson($array);

        return new self(
            is_array($array) ? json_encode($array) : $array,
            $code,
            array_merge(['Content-Type' => 'application/json'], $headers)
        );
    }

    /**
     * @param $view
     * @param array $data
     * @param int $code
     * @param array $headers
     * @return Response
     */
    public static function view($view, $data = [], $code = 200, Array $headers = [])
    {
        $view = View::make($view, $data);
        return new self(
            $view->__get('content'),
            $code,
            array_merge(['Content-Type' => 'text/html'], $headers)
        );

    }

    /**
     * @param $data
     * @param int $code
     * @param array $headers
     * @return Response
     * Make a response with type checking (json or html)
     */
    public static function make($data, $code = 200, Array $headers = [])
    {
        if ($data instanceof self) {
            return $data;
        }
        if (is_array($data) || is_object($data)) {
            return self::json($data, $code, $headers);
        } else {
            return self::raw($data, $code, $headers);
        }
    }

    /**
     * @param $key
     * @param null $value
     * Simple method for appending headers
     */
    protected function setHeader($key, $value = null)
    {
        $key = (!$value) ? $key : $key . ': ' . $value;

        ob_start();
        @header($key);
    }

    /**
     * @param $array
     * JSON checker
     */
    protected static function checkArrayValuesForJson(&$array)
    {
        if (is_array($array)) {
            foreach ($array as &$element) {
                if (is_object($element)) {
                    $element = json_decode((string)$element, true);
                } else {
                    if (is_array($element)) {
                        self::checkArrayValuesForJson($element);
                    }
                }
            }
        }
    }

    /**
     * Returning content
     */
    public function send()
    {
        echo $this->content;
        flush();
        exit;
    }

}