<?php
/**
 * Created by PhpStorm.
 * User: vic
 */

namespace App\Core;


use Exception;
use Throwable;

/**
 * Class FileNotFoundException
 * @package App\Core
 * Some separate exception class, useful extensions not implemented
 */
class FileNotFoundException extends Exception
{
    /**
     * FileNotFoundException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * For now fully repeats parent
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}