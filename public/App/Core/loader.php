<?php
/*
 * This is a loader file, with implemented constants, database configuration && autoloader
 */
/** Define Directories **/
defined('ROOT') ?: define('ROOT', dirname(dirname(__DIR__)));
defined('DS') ?: define('DS', DIRECTORY_SEPARATOR);
defined('WEBROOT') ?: define('WEBROOT', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . DS);
defined('ASSETS') ?: define('ASSETS', WEBROOT . 'assets' . DS);
defined('APP') ?: define('APP', ROOT . DS . 'App');
defined('CORE') ?: define('CORE', APP . DS . 'Core');
defined('CONT') ?: define('CONT', APP . DS . 'Controllers');
defined('MODEL') ?: define('MODEL', APP . DS . 'Models');
defined('VIEW') ?: define('VIEW', APP . DS . 'Views');
defined('TEMPLATES') ?: define('TEMPLATES', APP . DS . 'Templates');
defined('CONF') ?: define('CONF', APP . DS . 'Config');
defined('DIRECT') ?: define('DIRECT', true);

/** Database configuration */
$db_config = require(CONF . DS . 'db.php');
/** If there are few databases */
$db = $db_config['dev'];
defined('DB_HOSTNAME') ?: define('DB_HOSTNAME', $db['host']);
defined('DB_NAME') ?: define('DB_NAME', $db['db']);
defined('DB_USERNAME') ?: define('DB_USERNAME', $db['user']);
defined('DB_USERPWD') ?: define('DB_USERPWD', $db['password']);

/** Autoloader **/
spl_autoload_register(function ($class_name) {
    $parts = explode('\\', $class_name);
    array_walk($parts, function ($value) {
        return ucfirst($value);
    });
    $path = ROOT . DS . join($parts, DS) . '.php';
    if (file_exists($path)) {
        require($path);
    }
});