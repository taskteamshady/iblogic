document.addEventListener('DOMContentLoaded', function () {
    //Init modal windows in DOM
    var elements = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elements);

    //Api sending form validator
    function checkForm() {
        var emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
            email = document.getElementById('inputEmail').value,
            amount = document.getElementById('inputAmount').value;
        if (!emailRegex.test(email)) {
            showToast('Fill out correct email!');
            return false;
        }
        if (amount.length < 1) {
            showToast('Fill out the amount!');
            return false;
        } else {
            fixAmount();
        }
        return true;
    }
    //Function to fix floating values to double zeroes
    function fixAmount() {
        var amount = document.getElementById('inputAmount');
        var parts = amount.value.split('.').length;
        switch (parts) {
            case 1:
                amount.value = amount.value + '.00';
                break;
            case 2:
                var precision = amount.value.split('.')[1];
                switch (precision.length) {
                    case 0:
                        amount.value = amount.value + '00';
                        break;
                    case 1:
                        amount.value = amount.value + '0';
                        break;
                }
                break;
        }
    }
    //Show toast message function
    function showToast(text) {
        M.toast({html: text});
    }
    //Show modal window with header and text
    function showModal(header, text) {
        var instance = M.Modal.getInstance(document.querySelector('.modal')),
            head = document.querySelector('.modal h4'),
            body = document.querySelector('.modal div.description');
        body.innerText = text;
        head.innerText = header;
        instance.open();
    }
    //Show modal window with header and text
    function showReportInModal(header, html) {
        var instance = M.Modal.getInstance(document.querySelector('.modal')),
            head = document.querySelector('.modal h4'),
            body = document.querySelector('.modal div.description');
        body.innerHTML = html;
        head.innerText = header;
        instance.open();
    }

    function sendTransaction() {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            try {
                if (xhr.readyState === 4) {
                    switch (xhr.status) {
                        case 200:
                            var answer = JSON.parse(xhr.responseText);
                            var head = answer.message,
                                body = 'Your transaction id is ' + answer.transaction_id;
                            showModal(head, body);
                            document.querySelector('form').reset();
                            break;
                        case 500:
                            showModal('System error!', 'Something went wrong');
                            break;
                        case 403:
                            showModal('Forbidden', 'You have no access to the transaction!');
                            break;
                        default:
                            showModal('Awkward!', 'System does not answer');
                            break;

                    }
                }
            } catch (e) {
                console.log(e);
            }
        };
        xhr.open('POST', 'transaction');

        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        var body = 'email=' + encodeURIComponent(document.getElementById('inputEmail').value) +
            '&amount=' + encodeURIComponent(document.getElementById('inputAmount').value);
        xhr.send(body);
    }

    function getReport(type) {
        var xhr = new XMLHttpRequest(),
            types = {
                'byday': 'Report by days',
                'general': 'Report by month'
            };
        xhr.onreadystatechange = function () {
            try {
                if (xhr.readyState === 4) {
                    switch (xhr.status) {
                        case 200:
                            showReportInModal(types[type], xhr.responseText);
                            break;
                        case 500:
                            showModal('System error!', 'Something went wrong');
                            break;
                        case 403:
                            showModal('Forbidden', 'You have no access to the transaction!');
                            break;
                        default:
                            showModal('Awkward!', 'System does not answer');
                            break;
                    }
                }
            } catch (e) {
                console.log(e);
            }
        };
        xhr.open('POST', 'api/getReport');

        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        var body = 'type=' + encodeURIComponent(type);
        xhr.send(body);
    }

    var form = document.querySelector('form'),
        reportByMonthBtn = document.getElementById('reportByMonth'),
        reportByDaysBtn = document.getElementById('reportByDays'),
        inputAmount = document.getElementById('inputAmount'),
        modalClose = document.querySelector('a.modal-close');
    form.addEventListener('submit', function (e) {
        e.preventDefault();
        if(checkForm()) {
            sendTransaction();
            showToast('Sent');
        }
    });
    modalClose.addEventListener('click',function (e) {
        e.preventDefault();
    });
    reportByMonthBtn.addEventListener('click', function (e) {
        e.preventDefault();
        getReport('general');
    });
    reportByDaysBtn.addEventListener('click', function (e) {
        e.preventDefault();
        getReport('byday')
    });
    inputAmount.addEventListener('keypress', function (e) {
        if ((e.charCode < 48 || e.charCode > 57) && e.charCode !== 46) {
            e.preventDefault();
        }
        try {
            var parts = this.value.split('.');
            if (e.charCode === 46 && parts.length === 2) {
                e.preventDefault();
            } else {
                if(parts.length === 2 && parts[1].length === 2) {
                    e.preventDefault();
                }
            }
        } catch (e) {
            console.log("Can't split, will be: " + e);
        }
    });
});