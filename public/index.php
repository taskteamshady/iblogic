<?php

use App\Core\Router as Request;

require_once __DIR__ . "/App/Core/loader.php";
try {
    new Request();
} catch (Exception $e) {
    echo $e->getMessage(), '<br>Trace: ','<hr><pre>';
    print_r($e->getTrace()); echo '</pre>';
    exit();
}